use std::os::fd::AsRawFd;

use super::{
    capabilities::set_capabilities, config::ContainerOpts, errors::MyErr,
    hostname::set_container_hostname, mounts::set_mountpoint, namespaces::userns,
    syscalls::set_syscalls,
};
use nix::sched;
use nix::sys::signal;
use nix::unistd;
use std::ffi::CString;

const STACK_SIZE: usize = 1024 * 1024;

pub fn generate_child_process(config: &ContainerOpts) -> Result<unistd::Pid, MyErr> {
    let mut tmp_stack: [u8; STACK_SIZE] = [0; STACK_SIZE];
    // each flag will create a new namespace for the child process,
    let mut flags = sched::CloneFlags::empty();
    flags.insert(sched::CloneFlags::CLONE_NEWNS);
    flags.insert(sched::CloneFlags::CLONE_NEWCGROUP);
    flags.insert(sched::CloneFlags::CLONE_NEWPID);
    flags.insert(sched::CloneFlags::CLONE_NEWIPC);
    flags.insert(sched::CloneFlags::CLONE_NEWNET);
    flags.insert(sched::CloneFlags::CLONE_NEWUTS);

    unsafe {
        sched::clone(
            Box::new(|| child(config)),
            &mut tmp_stack,
            flags,
            Some(signal::Signal::SIGCHLD as i32),
        )
        .map_err(|e| e.into())
    }
}

fn child(config: &ContainerOpts) -> isize {
    if let Err(e) = setup_container_configurations(config) {
        log::error!("Error while configuring container: {e:?}");
        return -1;
    }

    if let Err(e) = unistd::close(config.fd_socket_child.as_raw_fd()) {
        log::error!("Error while closing socket: {e:?}");
        return -1;
    }

    if let Err(e) =
        unistd::execve::<CString, CString>(config.cmd.first().unwrap(), &config.cmd, &[])
    {
        log::error!("Error while trying to perform ecexve: {e:?}");
        return -1;
    }

    0
}

fn setup_container_configurations(config: &ContainerOpts) -> Result<(), MyErr> {
    set_container_hostname(&config.hostname)?;
    log::debug!("Container hostname is {}", &config.hostname);
    set_mountpoint(&config.mount_dir, &config.add_paths)?;
    userns(&config.fd_socket_child, config.uid)?;
    set_capabilities()?;
    set_syscalls()
}
