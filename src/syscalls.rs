use super::errors::{tryy, MyErr};
use libc::TIOCSTI;
use nix::{sched, sys::stat};

const EPERM: u16 = 1;

pub fn set_syscalls() -> Result<(), MyErr> {
    log::debug!("Refusing/Filtering unwanted syscalls");
    let s_isuid: u64 = stat::Mode::S_ISUID.bits().into();
    let s_isgid: u64 = stat::Mode::S_ISGID.bits().into();
    let clone_new_user: u64 = sched::CloneFlags::CLONE_NEWUSER.bits() as u64;

    let syscalls_refuse_ifcomp = [
        (syscallz::Syscall::chmod, 1, s_isuid),
        (syscallz::Syscall::chmod, 1, s_isgid),
        (syscallz::Syscall::fchmod, 1, s_isuid),
        (syscallz::Syscall::fchmod, 1, s_isgid),
        (syscallz::Syscall::fchmodat, 2, s_isuid),
        (syscallz::Syscall::fchmodat, 2, s_isgid),
        (syscallz::Syscall::unshare, 0, clone_new_user),
        (syscallz::Syscall::clone, 0, clone_new_user),
        (syscallz::Syscall::ioctl, 1, TIOCSTI),
    ];

    let syscalls_refused = [
        syscallz::Syscall::keyctl,
        syscallz::Syscall::add_key,
        syscallz::Syscall::request_key,
        syscallz::Syscall::mbind,
        syscallz::Syscall::migrate_pages,
        syscallz::Syscall::move_pages,
        syscallz::Syscall::set_mempolicy,
        syscallz::Syscall::userfaultfd,
        syscallz::Syscall::perf_event_open,
    ];

    let mut ctx = syscallz::Context::init_with_action(syscallz::Action::Allow)?;

    for (sc, idx, biteq) in syscalls_refuse_ifcomp {
        tryy!(ctx.set_rule_for_syscall(
            syscallz::Action::Errno(EPERM),
            sc,
            &[syscallz::Comparator::new(
                idx,
                syscallz::Cmp::MaskedEq,
                biteq,
                Some(biteq),
            )],
        ));
    }

    for sc in syscalls_refused {
        tryy!(ctx.set_action_for_syscall(syscallz::Action::Errno(EPERM), sc));
    }

    ctx.load()
        .map_err(|e| MyErr::new(file!(), line!(), &e.to_string()))
}
