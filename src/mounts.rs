use nix::{errno::Errno, mount, unistd};
use std::fs::{create_dir_all, remove_dir};
use std::path::{Path, PathBuf};

use super::{
    config::random_seed,
    errors::{tryy, MyErr},
};

const CHARS: &[u8] = b"ABCDEFGHIJKLMNOPQRSTUVWXYZ\
                       abcdefghijklmnopqrstuvwxyz\
                       0123456789";

pub fn set_mountpoint(mountpoint: &Path, add_paths: &[(PathBuf, PathBuf)]) -> Result<(), MyErr> {
    log::debug!("Remounting everything with MS_PRIVATE...");
    tryy!(mount_path(
        None,
        Path::new("/"),
        &[mount::MsFlags::MS_REC, mount::MsFlags::MS_PRIVATE],
    ));

    log::debug!("Making a temp directory and a bind mount there...");
    let new_root_name = format!("/tmp/try_container.{}", random_string(12));
    let new_root_path = Path::new(&new_root_name);
    tryy!(create_dir_all(new_root_path));
    tryy!(mount_path(
        Some(mountpoint),
        new_root_path,
        &[
            mount::MsFlags::MS_BIND,
            mount::MsFlags::MS_PRIVATE,
            mount::MsFlags::MS_RDONLY,
        ],
    ));

    log::debug!("Mounting additional paths");
    for (from_path, to) in add_paths {
        let to_path = new_root_path.join(to);
        tryy!(create_dir_all(&to_path));
        tryy!(mount_path(
            Some(from_path),
            &to_path,
            &[mount::MsFlags::MS_BIND, mount::MsFlags::MS_PRIVATE],
        ));
    }

    log::debug!("Pivoting root");
    let inner_mount_name = format!("oldroot.{}", random_string(6));
    let inner_mount_path = new_root_path.join(inner_mount_name.clone());
    tryy!(create_dir_all(&inner_mount_path));
    tryy!(unistd::pivot_root(new_root_path, &inner_mount_path));

    log::debug!("Unmounting old root");
    let old_root_path = Path::new(&inner_mount_name);
    tryy!(unistd::chdir(Path::new("/")));
    tryy!(mount::umount2(old_root_path, mount::MntFlags::MNT_DETACH));
    remove_dir(old_root_path).map_err(|e| MyErr::new(file!(), line!(), &e.to_string()))
}

pub fn clean_mounts(_root_path: &Path) -> Result<(), MyErr> {
    // unmount_path(root_path)
    Ok(())
}

fn random_string(n: usize) -> String {
    (0..n)
        .map(|_| {
            let idx = random_seed() as usize % CHARS.len();
            CHARS[idx] as char
        })
        .collect()
}

fn mount_path(
    path: Option<&Path>,
    mount_point: &Path,
    flags: &[mount::MsFlags],
) -> Result<(), Errno> {
    let mut ms_flags = mount::MsFlags::empty();
    for flag in flags {
        ms_flags.insert(*flag);
    }
    mount::mount::<Path, Path, Path, Path>(path, mount_point, None, ms_flags, None)
}
