mod capabilities;
mod child;
mod cli;
mod config;
mod container;
mod errors;
mod hostname;
mod ipc;
mod mounts;
mod namespaces;
mod resources;
mod syscalls;

fn main() -> Result<(), errors::MyErr> {
    env_logger::Builder::from_default_env()
        .format_timestamp_secs()
        .filter(None, log::LevelFilter::Debug)
        .init();

    let args = cli::parse_args()?;
    log::info!("{args:?}");
    container::start(args)
}
