use std::ffi::CString;
use std::os::fd::OwnedFd;
use std::path::PathBuf;

use super::{errors::MyErr, hostname::generate_hostname};

pub struct ContainerOpts {
    pub cmd: Vec<CString>,
    pub uid: u32,
    pub mount_dir: PathBuf,
    pub fd_socket_child: OwnedFd,
    pub hostname: String,
    pub add_paths: Vec<(PathBuf, PathBuf)>,
}

impl ContainerOpts {
    pub fn new(
        command: String,
        uid: u32,
        mount_dir: PathBuf,
        fd_socket_child: OwnedFd,
        add_paths: Option<Vec<(PathBuf, PathBuf)>>,
    ) -> Result<Self, MyErr> {
        let cmd: Result<Vec<CString>, std::ffi::NulError> =
            command.split_ascii_whitespace().map(CString::new).collect();

        Ok(Self {
            cmd: cmd?,
            uid,
            mount_dir,
            fd_socket_child,
            hostname: generate_hostname()?,
            add_paths: add_paths.unwrap_or_default(),
        })
    }
}

pub fn random_seed() -> u64 {
    std::hash::Hasher::finish(&std::hash::BuildHasher::build_hasher(
        &std::collections::hash_map::RandomState::new(),
    ))
}
