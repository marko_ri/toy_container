use super::errors::{tryy, MyErr};

use cgroups_rs::{self, cgroup_builder::CgroupBuilder, hierarchies, CgroupPid};
use nix::unistd;
use std::convert::TryInto;
use std::fs;

const MEM_LIMIT: i64 = 1024 * 1024 * 64;
const MAX_PID: cgroups_rs::MaxValue = cgroups_rs::MaxValue::Value(64);
const NOFILE_RLIMIT: u64 = 64;

// TODO: probably needs some fixing as some things are missing in cgroup.controllers (but for now good :))
// Maybe use Cgroup::new_with_specified_controllers (?)
pub fn restrict_resources(hostname: &str, pid: unistd::Pid) -> Result<(), MyErr> {
    log::debug!("Restricting resources for hostname: {hostname}");

    let cg = CgroupBuilder::new(hostname)
        .cpu()
        .shares(256)
        .done()
        .memory()
        .kernel_memory_limit(MEM_LIMIT)
        .memory_hard_limit(MEM_LIMIT)
        .done()
        .pid()
        .maximum_number_of_processes(MAX_PID)
        .done()
        .blkio()
        .weight(50)
        .done()
        .build(hierarchies::auto())?;

    let pid: u64 = pid.as_raw().try_into().unwrap();
    tryy!(cg.add_task_by_tgid(CgroupPid::from(pid)));

    rlimit::setrlimit(rlimit::Resource::NOFILE, NOFILE_RLIMIT, NOFILE_RLIMIT).map_err(|e| e.into())
}

// TODO: use delete from cgroups_rs (?)
pub fn clean_cgroups(hostname: &str) -> std::io::Result<()> {
    let path = fs::canonicalize(format!("/sys/fs/cgroup/{}", hostname))?;
    fs::remove_dir(path)
}
