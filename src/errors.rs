// beause question mark operator does not give file/line_num info
// TODO: logging gets messed up
macro_rules! tryy {
    ($e:expr) => {
        match $e {
            Ok(val) => val,
            Err(err) => {
                log::error!("{err}");
                return Err(MyErr::new(file!(), line!(), &err.to_string()));
            }
        }
    };
}
pub(crate) use tryy;

#[derive(Debug)]
pub struct MyErr {
    file: Option<&'static str>,
    line: Option<u32>,
    msg: String,
}

impl MyErr {
    pub fn new(file: &'static str, line: u32, msg: &str) -> Self {
        log::error!("File: {file}, Line: {line}: {msg}");
        Self {
            file: Some(file),
            line: Some(line),
            msg: msg.to_string(),
        }
    }
}

impl std::fmt::Display for MyErr {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        if self.file.is_some() && self.line.is_some() {
            write!(
                f,
                "File: {}, Line: {}: {}",
                self.file.unwrap(),
                self.line.unwrap(),
                self.msg
            )
        } else {
            write!(f, "{}", self.msg)
        }
    }
}

impl From<String> for MyErr {
    fn from(s: String) -> Self {
        Self {
            file: None,
            line: None,
            msg: s,
        }
    }
}

impl std::error::Error for MyErr {}

impl From<pico_args::Error> for MyErr {
    fn from(err: pico_args::Error) -> Self {
        log::error!("{err}");
        Self::from(err.to_string())
    }
}

impl From<std::ffi::NulError> for MyErr {
    fn from(err: std::ffi::NulError) -> Self {
        log::error!("{err}");
        Self::from(err.to_string())
    }
}

impl From<nix::errno::Errno> for MyErr {
    fn from(err: nix::errno::Errno) -> Self {
        log::error!("{err}");
        Self::from(err.to_string())
    }
}

impl From<std::io::Error> for MyErr {
    fn from(err: std::io::Error) -> Self {
        log::error!("{err}");
        Self::from(err.to_string())
    }
}

impl From<syscallz::Error> for MyErr {
    fn from(err: syscallz::Error) -> Self {
        log::error!("{err}");
        Self::from(err.to_string())
    }
}

impl From<cgroups_rs::error::Error> for MyErr {
    fn from(err: cgroups_rs::error::Error) -> Self {
        log::error!("{err}");
        Self::from(err.to_string())
    }
}
