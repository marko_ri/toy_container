use super::{
    child::generate_child_process,
    cli::Args,
    config::ContainerOpts,
    errors::{tryy, MyErr},
    ipc::generate_socketpair,
    mounts::clean_mounts,
    namespaces::handle_child_uid_map,
    resources::clean_cgroups,
    resources::restrict_resources,
};
use nix::sys::wait;
use nix::unistd;
use std::os::fd::AsRawFd;
use std::os::unix::io::OwnedFd;

// thread mode (cgroups) added in Linux 4.14
const MINIMAL_KERNEL_VERSION: KernelVersion = KernelVersion {
    major: 4,
    minor: 14,
};

#[derive(PartialEq, PartialOrd, Debug)]
struct KernelVersion {
    pub major: u8,
    pub minor: u8,
}

struct Container {
    fd_socket_parent: OwnedFd,
    config: ContainerOpts,
    child_pid: Option<unistd::Pid>,
}

impl Container {
    fn new(args: Args) -> Result<Self, MyErr> {
        let (fd_pair1, fd_pair2) = tryy!(generate_socketpair());
        let config =
            ContainerOpts::new(args.command, args.uid, args.mount, fd_pair1, args.add_paths)?;
        Ok(Self {
            fd_socket_parent: fd_pair2,
            config,
            child_pid: None,
        })
    }

    fn create(&mut self) -> Result<(), MyErr> {
        let pid = generate_child_process(&self.config)?;
        restrict_resources(&self.config.hostname, pid)?;
        handle_child_uid_map(pid, &self.fd_socket_parent)?;
        self.child_pid = Some(pid);
        Ok(())
    }

    fn clean_exit(&mut self) -> Result<(), MyErr> {
        log::debug!("Cleaning container");
        tryy!(clean_mounts(&self.config.mount_dir));
        tryy!(unistd::close(self.fd_socket_parent.as_raw_fd()));
        tryy!(unistd::close(self.config.fd_socket_child.as_raw_fd()));
        clean_cgroups(&self.config.hostname)
            .map_err(|e| MyErr::new(file!(), line!(), &e.to_string()))
    }
}

fn check_linux_version() -> Result<(), MyErr> {
    let host = nix::sys::utsname::uname()?;
    log::debug!("Linux release: {:?}", host.release());

    let mut version = host.release().to_str().unwrap().bytes();
    let major = version.next().unwrap() - b'0';
    let minor = version.nth(1).unwrap() - b'0';
    let version = KernelVersion { major, minor };
    // dbg!(&version);
    if version < MINIMAL_KERNEL_VERSION {
        return Err(MyErr::new(file!(), line!(), "Kernel version not supported"));
    }

    if host.machine() != "x86_64" {
        return Err(MyErr::new(file!(), line!(), "Architecture not supported"));
    }

    Ok(())
}

pub fn start(args: Args) -> Result<(), MyErr> {
    check_linux_version()?;
    let mut container = Container::new(args)?;

    match container.create() {
        Ok(_) => {
            if let Some(child_pid) = container.child_pid {
                wait_child(child_pid)?;
            }
            container.clean_exit()
        }
        Err(e) => {
            container.clean_exit()?;
            Err(e)
        }
    }
}

fn wait_child(child_pid: unistd::Pid) -> Result<wait::WaitStatus, MyErr> {
    log::debug!("Waiting for child (pid {:?}) to finish", child_pid);
    wait::waitpid(child_pid, None).map_err(|e| e.into())
}
