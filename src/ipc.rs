use nix::{errno::Errno, sys::socket};
use std::os::fd::{AsRawFd, OwnedFd};

pub fn generate_socketpair() -> Result<(OwnedFd, OwnedFd), Errno> {
    socket::socketpair(
        socket::AddressFamily::Unix,
        socket::SockType::SeqPacket,
        None,
        socket::SockFlag::SOCK_CLOEXEC,
    )
}

pub fn send_boolean(fd: &OwnedFd, boolean: bool) -> Result<usize, Errno> {
    let data: [u8; 1] = [boolean.into()];
    socket::send(fd.as_raw_fd(), &data, socket::MsgFlags::empty())
}

pub fn recv_boolean(fd: &OwnedFd) -> Result<bool, Errno> {
    let mut data: [u8; 1] = [0];
    socket::recv(fd.as_raw_fd(), &mut data, socket::MsgFlags::empty())?;
    Ok(data[0] == 1)
}
