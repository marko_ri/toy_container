use super::{config::random_seed, errors::MyErr};
use nix::unistd;

const HOSTNAME_NAMES: [&str; 8] = [
    "cat", "world", "coffee", "girl", "man", "book", "pinguin", "moon",
];

const HOSTNAME_ADJ: [&str; 16] = [
    "blue",
    "red",
    "green",
    "yellow",
    "big",
    "small",
    "tall",
    "thin",
    "round",
    "square",
    "triangular",
    "weird",
    "noisy",
    "silent",
    "soft",
    "irregular",
];

pub fn generate_hostname() -> Result<String, MyErr> {
    let name_idx = random_seed() as usize % HOSTNAME_NAMES.len();
    let name = HOSTNAME_NAMES[name_idx];
    let adj_idx = random_seed() as usize % HOSTNAME_ADJ.len();
    let adj = HOSTNAME_ADJ[adj_idx];
    Ok(format!("{name}-{adj}"))
}

pub fn set_container_hostname(hostname: &str) -> Result<(), MyErr> {
    unistd::sethostname(hostname).map_err(|e| e.into())
}
