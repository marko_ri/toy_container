use std::path::PathBuf;

use super::errors::MyErr;

const HELP: &str = "\
USAGE:
    toy_container [OPTIONS]

OPTIONS:
    --mount <mount-dir>        Directory to mount as root of the container
    --uid <uid>                User ID to create inside the container
    --command <cmd>            Command to execute inside the container
    --addpaths <path-list>     List of directories on host that will be mounted in container (see example)
    --help                     Prints help information

EXAMPLE:
    This program can be used like this:
    sudo ./toy_container --mount ./mountdir --uid 0 --command /bin/bash --addpaths '/lib64:/lib64 /lib:/lib'
";

#[derive(Debug)]
pub struct Args {
    pub mount: PathBuf,
    pub uid: u32,
    pub command: String,
    pub add_paths: Option<Vec<(PathBuf, PathBuf)>>,
}

pub fn parse_args() -> Result<Args, MyErr> {
    let mut pargs = pico_args::Arguments::from_env();

    // Help has a higher priority and should be handled separately.
    if pargs.contains("--help") {
        print!("{}", HELP);
        std::process::exit(0);
    }

    let args = Args {
        mount: pargs.value_from_os_str("--mount", parse_path)?,
        uid: pargs.value_from_str("--uid")?,
        command: pargs.value_from_str("--command")?,
        add_paths: pargs.opt_value_from_fn("--addpaths", parse_addpaths)?,
    };

    // ignore the remaining arguments
    let _remaining = pargs.finish();

    Ok(args)
}

fn parse_addpaths(mount_these: &str) -> Result<Vec<(PathBuf, PathBuf)>, &'static str> {
    let mut result = Vec::new();
    for from_to in mount_these.split_ascii_whitespace() {
        let mut pair = from_to.split(':');
        let from = PathBuf::from(pair.next().unwrap())
            .canonicalize()
            .expect("Cannot canonicalize path");
        let to = PathBuf::from(pair.next().unwrap())
            .strip_prefix("/")
            .expect("Cannot strip prefix from path")
            .to_path_buf();
        result.push((from, to));
    }
    Ok(result)
}

fn parse_path(path: &std::ffi::OsStr) -> Result<PathBuf, &'static str> {
    let result: PathBuf = path.into();
    if !result.exists() || !result.is_dir() {
        return Err("insert valid option for --mount");
    }

    Ok(result)
}
