use super::{
    errors::{tryy, MyErr},
    ipc::{recv_boolean, send_boolean},
};
use nix::{sched, unistd};
use std::fs::File;
use std::io::Write;
use std::os::fd::OwnedFd;

const USERNS_OFFSET: u64 = 10_000;
const USERNS_COUNT: u64 = 2_000;

// this is executed by the child process
pub fn userns(fd: &OwnedFd, uid: u32) -> Result<(), MyErr> {
    log::debug!("Setting up user name with UID {uid}");
    let has_userns = sched::unshare(sched::CloneFlags::CLONE_NEWUSER).is_ok();
    // Child process tells parent process if it supports user namespace
    tryy!(send_boolean(fd, has_userns));
    if tryy!(recv_boolean(fd)) {
        return Err(MyErr::new(file!(), line!(), "Usernamespace problem"));
    }

    // Parent did its job, so child can continue...
    let gid = unistd::Gid::from_raw(uid);
    let uid = unistd::Uid::from_raw(uid);
    unistd::setgroups(&[gid])?;
    unistd::setresgid(gid, gid, gid)?;
    unistd::setresuid(uid, uid, uid).map_err(|e| e.into())
}

// this is executed by the parent process
pub fn handle_child_uid_map(pid: unistd::Pid, fd: &OwnedFd) -> Result<usize, MyErr> {
    // If child supports user namespace isolation, parent process maps
    // UID / GID of the user namespace
    if tryy!(recv_boolean(fd)) {
        File::create(format!("/proc/{}/uid_map", pid.as_raw())).and_then(|mut uid_map| {
            uid_map.write_all(format!("0 {} {}", USERNS_OFFSET, USERNS_COUNT).as_bytes())
        })?;
        File::create(format!("/proc/{}/gid_map", pid.as_raw())).and_then(|mut gid_map| {
            gid_map.write_all(format!("0 {} {}", USERNS_OFFSET, USERNS_COUNT).as_bytes())
        })?;
    } else {
        log::info!("No user namespace set up from child process");
    }

    log::debug!("Child UID/GID map done, sending signal to child to continue...");
    send_boolean(fd, false).map_err(|e| MyErr::new(file!(), line!(), &e.to_string()))
}
