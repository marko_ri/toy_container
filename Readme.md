
Toy container in Rust, based on the article: [Writing a container in rust](https://litchipi.site/serie/containers_in_rust)

And videos:

- [An introduction to control groups (cgroups) version 2](https://www.youtube.com/watch?v=kcnFQgg9ToY)
- [Diving deeper into control groups (cgroups) v2](https://www.youtube.com/watch?v=Clr_MQwaJtA)
- [Understanding Linux user namespaces](https://www.youtube.com/watch?v=XgThPoL9mPE)
